class LinksExtractorListener
  def self.slack_message_stored(params)
    Rails.logger.info "event received: #{params}\n"
    slackMessage = SlackMessage.find_by_id(params[:id])
    LinksExtractorService.new(slackMessage.data).extract_links unless slackMessage.data.nil?
  end
end
