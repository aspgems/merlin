class ApiClientController < ApplicationController
  require 'json'

  skip_before_action :verify_authenticity_token

  def slack_webhook
    json = request.raw_post
    message = SlackMessage.new(data: JSON.parse(json))
    if message.save
      render status: 200, json: {
        message: "Succesfully stored"
      }.to_json
    else
      render status: 500, json: {
        message: "Error: Saving in database"
      }.to_json
    end
  end
end
