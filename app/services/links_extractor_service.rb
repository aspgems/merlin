require 'metainspector'

class LinksExtractorService
    attr_reader :json

    def initialize(json)
        @json = json
    end

    def extract_links 
        mensaje = json['event']['text']
        regex_string = '(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-?=%.]+'
        regex = Regexp.new regex_string
        links = mensaje.scan(regex)

        links.each do |item|
            page = MetaInspector.new(item)
            message = Link.new(url: item, title: page.title, description: page.description)
            if message.save
                Rails.logger.info 'SAVED!'
            else
                Rails.logger.info 'FAIL SAVING!'
            end
        end
    end
end