# == Schema Information
#
# Table name: links
#
#  id          :bigint(8)        not null, primary key
#  url         :text
#  title       :text
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Link < ApplicationRecord
   
end
