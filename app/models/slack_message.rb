# == Schema Information
#
# Table name: slack_messages
#
#  id         :bigint(8)        not null, primary key
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SlackMessage < ApplicationRecord
  include Wisper::Publisher

  #Callbacks

  after_commit :broadcast_after_create, on: :create

  def broadcast_after_create
    broadcast(:slack_message_stored, id: self.id)
  end
end
