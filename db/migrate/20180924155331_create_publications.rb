class CreatePublications < ActiveRecord::Migration[5.2]
  def change
    create_table :publications do |t|
      t.jsonb :data

      t.timestamps
    end
  end
end
