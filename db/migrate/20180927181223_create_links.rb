class CreateLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :links do |t|
      t.text :url
      t.text :title
      t.text :description

      t.timestamps
    end
  end
end
