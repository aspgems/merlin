class RenamePublicationsToSlackMessage < ActiveRecord::Migration[5.2]
  def change
    rename_table :publications, :slack_messages
  end
end
