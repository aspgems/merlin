Rails.application.routes.draw do
  get 'welcome/index'

  root 'welcome#index'

  #API integration
  post "/slack_webhook" => 'api_client#slack_webhook'
end
